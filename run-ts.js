const os = require('os')
if (os.platform() != 'linux') {
  const quickfix = require('/home/T9A090/programs/lsp/node_modules/typescript-language-server/lib/quickfix');
  const protocol = require('/home/T9A090/programs/lsp/node_modules/typescript-language-server/lib/protocol-translation');
  const URI = require('/home/T9A090/programs/lsp/node_modules/vscode-uri');
  const old_uri = URI.default.file;
  const old_provideQuickFix = quickfix.provideQuickFix;
  const old_pathToUri = protocol.pathToUri;
  const old_uriToPath = protocol.uriToPath;

  function processPath(path) {
    let result = path.replace(/\\/g, '/');
    result = result.replace(/^c:/g, '/cygdrive/c')
    result = result.replace(/^\/cygdrive\/c\/Users\/T9A090/g, '/home/T9A090')
    const p = /^\/cygdrive\/c/g
    while (result.match(p)) {
      result = result.replace(p, '');
    }
    result = result.replace(/^\/home\/T9A090/g, '');
    result = '/home/T9A090' + result;
    return result;
  }

  quickfix.provideQuickFix = function(response, result, documents) {
    if (!response || !response.body) {
      return;
    }
    const r = /^import([^'"]+)(['"])[^'"]+node_modules\/([^'"]+)\2/g;
    for (const fix of response.body){
      for (const change of fix.changes) {
        for (const text of change.textChanges) {
          text.newText = text.newText.replace(r, "import$1$2$3$2");
        }
      }
    }

    old_provideQuickFix(response, result, documents);
  }

  URI.default.file = function(path) {
    return old_uri(processPath(path));
  }

  protocol.pathToUri = function(path, documents) {
    return old_pathToUri(processPath(path), documents);
  }

  protocol.uriToPath = function(uri) {
    return processPath(old_uriToPath(uri));
  }
}

require('/home/T9A090/programs/lsp/node_modules/typescript-language-server/lib/cli')
